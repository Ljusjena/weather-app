import React from 'react';
import { Image, Text } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';


const GooglePlacesInput = () => {
  return (
    <GooglePlacesAutocomplete
      placeholder='Search'
      fetchDetails= {true}
      listViewDisplayed={true} 
      onPress={(data, details = null) => {
        console.log(data, details);
      }}
      query={{
        key: 'AIzaSyDygqMuKdwaILDQ8OaJ6EnwasdXqthsp44',
        language: 'en',
      }}
    />
  );
};
export default GooglePlacesInput;